from setuptools import find_packages, setup

package_name = 'ur_tip_controller'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='kaijunge',
    maintainer_email='kai.junge@epfl.ch',
    description='TODO: Package description',
    license='Apache-2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'ur_tip_controller_node = ur_tip_controller.ur_tip_controller_node:main',
            'ur_teleop_test_node = ur_tip_controller.ur_teleop_test_node:main',
            'ur_simple_node = ur_tip_controller.ur_simple_node:main'
        ],
    },
)
