import rclpy
from rclpy.node import Node
import rtde_control
import rtde_receive
from sensor_msgs.msg import Joy
from helper_functions.gamepad_functions import GamepadFunctions
from std_msgs.msg import Float64MultiArray
from scipy.spatial.transform import Rotation as R
from scipy.spatial.transform import Slerp
from copy import deepcopy as cp
from helper_functions.utility import broadcast_tf
from tf2_ros import TransformBroadcaster
import numpy as np
from helper_functions.utility import Key

class URTipControllerNode(Node):

    def __init__(self):
        super().__init__('ur_tip_controller_node')

        self.gp = GamepadFunctions()

        arm_IP = "192.168.1.10"
        self.ctrl = rtde_control.RTDEControlInterface(arm_IP)
        self.recv = rtde_receive.RTDEReceiveInterface(arm_IP)

        self.ctrl.setTcp([0,0,0,0,0,0])
        self.ctrl.setPayload(0.01, [0, 0, 0.01])
        self.ctrl.endTeachMode()

        # Timer callbacks
        self.ur_true_pose = self.create_timer(0.001, self.ur_pose_callback) 
        self.move_ur = self.create_timer(0.01, self.ur_motion_callback) 
        self.freedrive = self.create_timer(0.01, self.freedrive_callback) 

        # TF related
        self.tf_broadcaster = TransformBroadcaster(self)
        
        # Publishers
        self.ur_current_pose_publisher = self.create_publisher(Float64MultiArray, '/franka/pose', 10)
        
        # Subscribers
        self.ur_pose_demand_subscriber = self.create_subscription(Float64MultiArray, '/franka/pose_demand', self.ur_pose_demand_callback, rclpy.qos.qos_profile_sensor_data)
        self.gamepad_subscriber = self.create_subscription(Joy, '/gamepad', self.gamepad_callback, rclpy.qos.qos_profile_sensor_data)

        self.key = Key()

        # Variables
        self.ur_demand_pose = None

        print("UR tip controller started")

    # This constantly gets the current pose of the UR
    def ur_pose_callback(self):
        raw_pose = self.recv.getActualTCPPose()
        self.raw = raw_pose

        base_rot_m = R.from_euler("z", 0, degrees=True).as_matrix() # 135
        raw_rot_m = R.from_rotvec(raw_pose[3:], degrees=False).as_matrix()

        trans = np.matmul(base_rot_m, raw_pose[:3])
        quat_m = np.matmul(base_rot_m, raw_rot_m)
        quat = R.from_matrix(quat_m).as_quat()

        broadcast_tf("world", "ur", trans, quat, self.tf_broadcaster, self.get_clock().now().to_msg())

        msg = Float64MultiArray()
        msg.data = list(trans) + list(quat)
        self.ur_current_pose_publisher.publish(msg)

        if self.ur_demand_pose is None:
            self.ur_demand_pose = raw_pose

    # This constantly gets any demand pose and keeps it saved in a shared variable
    def ur_pose_demand_callback(self, msg:Float64MultiArray):
        reverse_base_rot_m = R.from_euler("z", 0, degrees=True).as_matrix() #-135
        demand_quat_m = R.from_quat(msg.data[3:]).as_matrix()
        demand_trans = msg.data[:3]

        quat_m = np.matmul(reverse_base_rot_m, demand_quat_m)
        rot = R.from_matrix(quat_m).as_rotvec()

        trans = np.matmul(reverse_base_rot_m, demand_trans)

        self.ur_demand_pose = list(trans) + list(rot)

    def ur_motion_callback(self):
        if self.ur_demand_pose is None:
            return
        
        # Velocity limiting
        trans_vector = np.array([self.ur_demand_pose[0] - self.raw[0], 
                                self.ur_demand_pose[1] - self.raw[1],
                                self.ur_demand_pose[2] - self.raw[2]])
        
        trans_vector_norm = np.linalg.norm(trans_vector)
        max_norm = 0.001
        if trans_vector_norm > max_norm: # 0.1mm / 2ms = 0.05 m/s = 5cm/s
            trans_vector = (trans_vector / trans_vector_norm) * max_norm

        target = cp(self.raw)
        for i in range(3):
            target[i] += trans_vector[i]
            target[i+3] = self.ur_demand_pose[i+3]
        
        velocity = 0.1; acceleration = 0.1; dt_temp = 1.0/500; lookahead_time = 0.05; gain = 100
        pose = target

        if self.gp.button_data["L1"] == 1:
            self.t_start = self.recv.initPeriod()
            self.ctrl.servoL(pose, velocity, acceleration, dt_temp, lookahead_time, gain)
            self.ctrl.waitPeriod(self.t_start)

    def gamepad_callback(self, gamepad_raw_msg:Joy):
        self.gp.convert_joy_msg_to_dictionary(gamepad_raw_msg)

    def freedrive_callback(self):
        # return
        if self.gp.if_button_pressed("circle"):
            print("Freedrive!!")
            self.ctrl.teachMode()

        if self.gp.if_button_pressed("cross"):
            print("End Freedrive!!")
            self.ctrl.endTeachMode()
        
            
def main(args=None):
    rclpy.init(args=args)

    ur_tip_controller_node = URTipControllerNode()

    rclpy.spin(ur_tip_controller_node)

    ur_tip_controller_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()