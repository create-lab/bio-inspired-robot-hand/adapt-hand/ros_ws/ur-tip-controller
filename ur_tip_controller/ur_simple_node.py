import rclpy
from rclpy.node import Node
import rtde_control
import rtde_receive
from sensor_msgs.msg import Joy
from helper_functions.gamepad_functions import GamepadFunctions
from std_msgs.msg import Float64MultiArray
from scipy.spatial.transform import Rotation as R
from scipy.spatial.transform import Slerp
from copy import deepcopy as cp
from helper_functions.utility import broadcast_tf
from tf2_ros import TransformBroadcaster
import numpy as np
from helper_functions.utility import Key

class URSimpleNode(Node):

    def __init__(self):
        super().__init__('ur_simple_node')

        self.gp = GamepadFunctions()

        arm_IP = "192.168.1.20"
        self.ctrl = rtde_control.RTDEControlInterface(arm_IP)
        self.recv = rtde_receive.RTDEReceiveInterface(arm_IP)

        self.key = Key()

        # Timer callbacks
        self.move_ur = self.create_timer(0.00001, self.ur_motion_callback) 
        self.gamepad_subscriber = self.create_subscription(Joy, '/gamepad', self.gamepad_callback, 10)

        self.ur_pose_demand_subscriber = self.create_subscription(Float64MultiArray, '/ur/pose_demand', self.ur_pose_demand_callback, 10)

        # Variables
        self.curr_pose = cp(self.recv.getActualTCPPose())

        print("UR tip controller started")

    def ur_pose_demand_callback(self, msg:Float64MultiArray):
        self.curr_pose = list(msg.data)
        
    def ur_motion_callback(self):
        if self.key.keyPress == "w":
            self.curr_pose[2] += 0.001
        elif self.key.keyPress == "s":
            self.curr_pose[2] -= 0.001

        velocity = 0.1; acceleration = 0.1; dt_temp = 1.0/500; lookahead_time = 0.1; gain = 300
        t_start = self.recv.initPeriod()
        self.ctrl.servoL(self.curr_pose, velocity, acceleration, dt_temp, lookahead_time, gain)
        self.ctrl.waitPeriod(t_start)

    def gamepad_callback(self, gamepad_raw_msg:Joy):
        self.gp.convert_joy_msg_to_dictionary(gamepad_raw_msg)

def main(args=None):
    rclpy.init(args=args)

    ur_simple_node = URSimpleNode()

    rclpy.spin(ur_simple_node)

    ur_simple_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()