import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Joy
from helper_functions.gamepad_functions import GamepadFunctions
from std_msgs.msg import Float64MultiArray
import numpy as np

class URTipControllerNode(Node):

    def __init__(self):
        super().__init__('ur_tip_controller_node')

        self.gp = GamepadFunctions()

        # Callback
        self.mainloop = self.create_timer(0.01, self.mainloop_callback) 
        
        # Publishers
        self.ur_pose_demand_publisher = self.create_publisher(Float64MultiArray, '/franka/pose_demand', 10)
        
        # Subscribers
        self.ur_current_pose_subscriber = self.create_subscription(Float64MultiArray, '/franka/pose', self.ur_pose_callback, 10)
        self.gamepad_subscriber = self.create_subscription(Joy, '/gamepad', self.gamepad_callback, 10)
        
        # Variables
        self.target_pose = None

        print("UR teleop node started")

    def mainloop_callback(self):
        
        if self.target_pose is None:
            return
        
        if abs(self.gp.axis_data["Rx"]) > 0.2:
            self.target_pose[2] += self.gp.axis_data["Rx"] * 0.001

        msg = Float64MultiArray()
        msg.data = self.target_pose
        self.ur_pose_demand_publisher.publish(msg)

    def ur_pose_callback(self, msg:Float64MultiArray):
        if self.target_pose is None:
            self.target_pose = msg.data

    def gamepad_callback(self, gamepad_raw_msg:Joy):
        self.gp.convert_joy_msg_to_dictionary(gamepad_raw_msg)
            
def main(args=None):
    rclpy.init(args=args)

    ur_tip_controller_node = URTipControllerNode()

    rclpy.spin(ur_tip_controller_node)

    ur_tip_controller_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()