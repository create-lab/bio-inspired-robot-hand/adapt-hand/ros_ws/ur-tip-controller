import rtde_control
import rtde_receive
from helper_functions.utility import Key
from copy import deepcopy as cp

key = Key()

arm_IP = "192.168.1.20"
ctrl = rtde_control.RTDEControlInterface(arm_IP)
recv = rtde_receive.RTDEReceiveInterface(arm_IP)

curr_pose = cp(recv.getActualTCPPose())

while 1:
    if key.keyPress == "w":
        curr_pose[2] += 0.001
    elif key.keyPress == "s":
        curr_pose[2] -= 0.001

    velocity = 0.1; acceleration = 0.1; dt_temp = 1.0/500; lookahead_time = 0.1; gain = 300
    t_start = recv.initPeriod()
    ctrl.servoL(curr_pose, velocity, acceleration, dt_temp, lookahead_time, gain)
    ctrl.waitPeriod(t_start)
